# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * website_sale_wishlist
# 
# Translators:
# Sergio Zanchetta <primes2h@gmail.com>, 2018
# Martin Trigaux, 2018
# Paolo Valier, 2018
# Pietro Della Notte <pdellanotte@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~11.5\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-08-24 09:05+0000\n"
"PO-Revision-Date: 2018-08-24 09:05+0000\n"
"Last-Translator: Pietro Della Notte <pdellanotte@gmail.com>, 2018\n"
"Language-Team: Italian (https://www.transifex.com/odoo/teams/41243/it/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: it\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: website_sale_wishlist
#: model:ir.ui.view,arch_db:website_sale_wishlist.header
msgid ""
"<i class=\"fa fa-heart\"/>\n"
"                        Wishlist"
msgstr ""

#. module: website_sale_wishlist
#: model:ir.ui.view,arch_db:website_sale_wishlist.product_wishlist
msgid "<small><i class=\"fa fa-trash-o\"/> Remove</small>"
msgstr "<small><i class=\"fa fa-trash-o\"/> Rimuovi</small>"

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_product_wishlist__active
msgid "Active"
msgstr "Attivo"

#. module: website_sale_wishlist
#: model:ir.ui.view,arch_db:website_sale_wishlist.product_wishlist
msgid "Add <span class=\"d-none d-md-inline\">to Cart</span>"
msgstr ""

#. module: website_sale_wishlist
#: model:ir.ui.view,arch_db:website_sale_wishlist.product_wishlist
msgid "Add product to my cart but keep it in my wishlist"
msgstr ""

#. module: website_sale_wishlist
#: model:ir.ui.view,arch_db:website_sale_wishlist.add_to_wishlist
#: model:ir.ui.view,arch_db:website_sale_wishlist.product_add_to_wishlist
msgid "Add to Wishlist"
msgstr "Aggiungi alla Wishlist"

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_product_wishlist__create_date
msgid "Added Date"
msgstr ""

#. module: website_sale_wishlist
#: model:ir.model,name:website_sale_wishlist.model_res_partner
msgid "Contact"
msgstr "Contatto"

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_product_wishlist__create_uid
msgid "Created by"
msgstr "Creato da"

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_product_wishlist__currency_id
msgid "Currency"
msgstr "Valuta"

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_product_wishlist__display_name
msgid "Display Name"
msgstr "Nome visualizzato"

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_product_wishlist__id
msgid "ID"
msgstr "ID"

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_product_wishlist____last_update
msgid "Last Modified on"
msgstr "Data di ultima modifica"

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_product_wishlist__write_uid
msgid "Last Updated by"
msgstr "Ultima modifica di"

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_product_wishlist__write_date
msgid "Last Updated on"
msgstr "Ultima modifica il"

#. module: website_sale_wishlist
#: model:ir.ui.view,arch_db:website_sale_wishlist.product_wishlist
msgid "My Wishlist"
msgstr ""

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_product_wishlist__partner_id
msgid "Owner"
msgstr "Proprietario"

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_product_wishlist__price
msgid "Price"
msgstr "Prezzo"

#. module: website_sale_wishlist
#: model:ir.model.fields,help:website_sale_wishlist.field_product_wishlist__price
msgid "Price of the product when it has been added in the wishlist"
msgstr ""

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_product_wishlist__pricelist_id
msgid "Pricelist"
msgstr "Listino prezzi"

#. module: website_sale_wishlist
#: model:ir.model.fields,help:website_sale_wishlist.field_product_wishlist__pricelist_id
msgid "Pricelist when added"
msgstr ""

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_product_wishlist__product_id
msgid "Product"
msgstr "Prodotto"

#. module: website_sale_wishlist
#: model:ir.ui.view,arch_db:website_sale_wishlist.product_wishlist
msgid "Product image"
msgstr ""

#. module: website_sale_wishlist
#: model:ir.ui.view,arch_db:website_sale_wishlist.product_wishlist
msgid "Shop Wishlist"
msgstr ""

#. module: website_sale_wishlist
#: model:ir.model,name:website_sale_wishlist.model_res_users
msgid "Users"
msgstr "Utenti"

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_product_wishlist__website_id
msgid "Website"
msgstr "Sito Web"

#. module: website_sale_wishlist
#: model:ir.model.fields,field_description:website_sale_wishlist.field_res_partner__wishlist_ids
#: model:ir.model.fields,field_description:website_sale_wishlist.field_res_users__wishlist_ids
msgid "Wishlist"
msgstr ""

#. module: website_sale_wishlist
#: model:ir.model,name:website_sale_wishlist.model_ir_autovacuum
msgid "ir.autovacuum"
msgstr "ir.autovacuum"

#. module: website_sale_wishlist
#: model:ir.model,name:website_sale_wishlist.model_product_wishlist
msgid "product.wishlist"
msgstr ""
